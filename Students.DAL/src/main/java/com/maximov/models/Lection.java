package com.maximov.models;

public class Lection extends Entity {
    private String subject;
    private String text;
    private String name;

    public Lection() {
    }

    public Lection(String subject, String text, String name) {
        this.subject = subject;
        this.text = text;
        this.name = name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return super.toString() + " subject: " + this.subject;
    }
}
