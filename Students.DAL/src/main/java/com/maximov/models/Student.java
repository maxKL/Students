package com.maximov.models;

import java.util.Date;

public class Student extends Entity {
    private String name;
    private Date birthdate;
    private boolean sex;
    private int groupId;

    public Student(){

    }

    public Student(String name, Date birthdate, boolean sex, int groupId) {
        this.name = name;
        this.birthdate = birthdate;
        this.sex = sex;
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public boolean isSex() {
        return sex;
    }

    public void setSex(boolean sex) {
        this.sex = sex;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    @Override
    public String toString() {
        return super.toString() + " name: " + this.name;
    }
}
