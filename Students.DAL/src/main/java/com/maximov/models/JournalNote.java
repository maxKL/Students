package com.maximov.models;

import java.util.Date;

public class JournalNote extends Entity {
    private int studentId;
    private int lectionId;
    private Date date;

    public JournalNote() {
    }

    public JournalNote(int studentId, int lectionId, Date date) {
        this.studentId = studentId;
        this.lectionId = lectionId;
        this.date = date;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getLectionId() {
        return lectionId;
    }

    public void setLectionId(int lectionId) {
        this.lectionId = lectionId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
