package com.maximov.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {
    private static String url = "jdbc:postgresql://localhost:5432/StudentsDB";
    private static String login = "postgres";
    private static String password = "08051992";
    private static Connection conn;


    private ConnectionManager() {
    }

    public static synchronized Connection getConnection() throws SQLException {
        if(conn == null) {
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            conn = DriverManager.getConnection(url, login, password);
        }
        return conn;
    }

}
