package com.maximov;

import com.maximov.models.Entity;
import com.maximov.models.Student;
import com.maximov.repository.GroupRepository;
import com.maximov.repository.Repository;
import com.maximov.repository.StudentRepository;

import java.sql.*;
import java.util.List;

public class Main {

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        StudentRepository studentRepository = new StudentRepository();
//        studentRepository.insert(new Student("Barbara", new Date(1961, 7, 19 ), false, 1));
//        studentRepository.insert(new Student("Chesterton", new Date(1961, 7, 19 ), true, 1));

        Student student = studentRepository.selectById(2);
        System.out.println("Before Update:");
        showEntity(student);
        student.setName("Viky");
        studentRepository.update(student);
        student = studentRepository.selectById(2);
        System.out.println("After Update:");
        showEntity(student);

        studentRepository.delete(3);

        List students = studentRepository.selectAll();
        System.out.println("Students:");
        showEntities(students);

        Repository groupRepository = new GroupRepository();
        List groups = groupRepository.selectAll();
        System.out.println("Groups:");
        showEntities(groups);

    }



    private static void showEntities(List<Entity> entities){
        for(Entity entity : entities){
            showEntity(entity);
        }
    }

    private static void showEntity(Entity entity){
        System.out.println(entity.toString());
    }
}
