package com.maximov.repository;

import com.maximov.models.Student;

import java.sql.*;

public class StudentRepository extends Repository<Student> {
    public StudentRepository() throws SQLException {
    }

    @Override
    protected String getTableName() {
        return "student";
    }

    @Override
    protected String[] getFieldsNames() {
        return new String[]{ "name", "birthdate", "sex", "group_id" };
    }

    @Override
    protected void setEntityParams(Student item, PreparedStatement prepareStatement) throws SQLException {
        prepareStatement.setString(1, item.getName());
        prepareStatement.setDate(2, (Date) item.getBirthdate());
        prepareStatement.setBoolean(3, item.isSex());
        prepareStatement.setInt(4, item.getGroupId());
    }

    @Override
    protected Student transformSelectResultItem(ResultSet resultSet) throws SQLException {
        Student student = new Student();
        setId(student, resultSet);
        student.setName(resultSet.getString("name"));
        student.setBirthdate(resultSet.getDate("birthdate"));
        student.setSex(resultSet.getBoolean("sex"));
        student.setGroupId(resultSet.getInt("group_id"));
        return student;
    }
}
