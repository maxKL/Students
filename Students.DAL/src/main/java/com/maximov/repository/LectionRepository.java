package com.maximov.repository;

import com.maximov.models.Entity;
import com.maximov.models.Lection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LectionRepository extends Repository<Lection> {
    public LectionRepository() throws SQLException {
    }

    @Override
    protected String getTableName() {
        return "lection";
    }

    @Override
    protected String[] getFieldsNames() {
        return new String[]{ "subject", "text", "name" };
    }

    @Override
    protected void setEntityParams(Lection item, PreparedStatement prepareStatement) throws SQLException {
        prepareStatement.setString(1, item.getSubject());
        prepareStatement.setString(2, item.getText());
        prepareStatement.setString(3, item.getName());
    }

    @Override
    protected Lection transformSelectResultItem(ResultSet resultSet) throws SQLException {
        Lection lection = new Lection();
        setId(lection, resultSet);
        lection.setSubject(resultSet.getString("subject"));
        lection.setText(resultSet.getString("text"));
        lection.setName(resultSet.getString("name"));

        return null;
    }
}
