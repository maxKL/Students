package com.maximov.repository;

import com.maximov.models.Entity;
import com.maximov.utils.ConnectionManager;

import java.sql.*;
import java.util.*;

public abstract class Repository<T extends Entity> {
    private final String tableName;
    private String fieldsString;
    private String paramsString;
    private String updateFieldsString;

    protected Connection connection;

    protected Repository() throws SQLException {
        connection = ConnectionManager.getConnection();
        tableName = String.format("public.\"%s\"", getTableName());
        String[] fieldsNames = getFieldsNames();
        fieldsString = "";
        paramsString = "";
        updateFieldsString = "";
        for(int i = 0; i < fieldsNames.length; i++){
            String end =(i < fieldsNames.length - 1 ? "," : "");

            fieldsString += fieldsNames[i] + end;
            paramsString += "?" + end;
            updateFieldsString += fieldsNames[i] + "=?" + end;
        }

    }

    protected abstract String getTableName();

    protected abstract String[] getFieldsNames();


    public int insert(T item){
        String sqlQuery = getInsertQuery();
        try {
            PreparedStatement prepareStatement = connection.prepareStatement(sqlQuery);
            setEntityParams(item, prepareStatement);
            return prepareStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }

    }

    protected String getInsertQuery() {
        return String.format("INSERT INTO %s (%s) VALUES (%s);",
                tableName, fieldsString, paramsString);
    }

    public int update(T item){
        String sqlQuery = getUpdateQuery();
        try {
            PreparedStatement prepareStatement = connection.prepareStatement(sqlQuery);
            setEntityParams(item, prepareStatement);
            prepareStatement.setInt(this.getFieldsNames().length + 1, item.getId());
            return prepareStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    protected String getUpdateQuery() {
        return String.format("UPDATE %s SET %s WHERE id = ?",
                tableName, updateFieldsString);
    }

    protected abstract void setEntityParams(T item, PreparedStatement prepareStatement) throws SQLException;

    public int delete(int id){
        String sqlQuery = getDeleteQuery();
        try {
            PreparedStatement prepareStatement = connection.prepareStatement(sqlQuery);
            prepareStatement.setInt(1, id);
            return prepareStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    protected String getDeleteQuery() {
        return String.format("DELETE FROM %s WHERE id = ?", tableName);
    }

    public List<T> selectAll(){
        String sqlQuery = getSelectAllQuery();
        List<T> resultList = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);

            while (resultSet.next()){
                T item = transformSelectResultItem(resultSet);
                resultList.add(item);
            }
            return resultList;
        } catch (SQLException e) {
            e.printStackTrace();
            return resultList;
        }
    }

    protected String getSelectAllQuery() {
        return String.format("SELECT * FROM %s", tableName);
    }

    public T selectById(int id){
        String sqlQuery = getSelectByIdQuery();
        try {
            PreparedStatement prepareStatement = connection.prepareStatement(sqlQuery);
            prepareStatement.setInt(1, id);
            ResultSet resultSet = prepareStatement.executeQuery();
            return resultSet.next() ? transformSelectResultItem(resultSet) : null;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    protected String getSelectByIdQuery() {
        return String.format("SELECT * FROM %s WHERE id = ?", tableName);
    }

    protected abstract T transformSelectResultItem(ResultSet resultSet) throws SQLException;

    protected void setId(T item, ResultSet resultSet) throws SQLException {
        item.setId(resultSet.getInt("id"));
    }
}
