package com.maximov.repository;

import com.maximov.models.JournalNote;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JournalRepository extends Repository<JournalNote> {
    public JournalRepository() throws SQLException {
    }

    @Override
    protected String getTableName() {
        return "Journal";
    }

    @Override
    protected String[] getFieldsNames() {
        return new String[] { "student_id", "lection_id", "date" };
    }

    @Override
    protected void setEntityParams(JournalNote item, PreparedStatement prepareStatement) throws SQLException {
        prepareStatement.setInt(1, item.getStudentId());
        prepareStatement.setInt(2, item.getLectionId());
        prepareStatement.setDate(3, (Date) item.getDate());
    }

    @Override
    protected JournalNote transformSelectResultItem(ResultSet resultSet) throws SQLException {
        JournalNote note = new JournalNote();
        note.setStudentId(resultSet.getInt("student_id"));
        note.setLectionId(resultSet.getInt("lection_id"));
        note.setDate(resultSet.getDate("date"));
        return null;
    }
}
