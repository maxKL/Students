package com.maximov.repository;

import com.maximov.models.Group;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GroupRepository extends Repository<Group> {
    public GroupRepository() throws SQLException {
    }

    @Override
    protected String getTableName() {
        return "group";
    }

    @Override
    protected String[] getFieldsNames() {
        return new String[]{ "name" };
    }

    @Override
    protected void setEntityParams(Group item, PreparedStatement prepareStatement) throws SQLException {
        prepareStatement.setString(1, item.getName());
    }

    @Override
    protected Group transformSelectResultItem(ResultSet resultSet) throws SQLException {
        Group group = new Group();
        setId(group, resultSet);
        group.setName(resultSet.getString("name"));
        return group;
    }
}
