<%@ page import="com.maximov.models.Student" %>
<%@ taglib prefix="c"
           uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: maxim_000
  Date: 22.02.2017
  Time: 12:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table>
    <tr>
        <td>Имя</td>
        <td>Дата рождения</td>
    </tr>
    <c:forEach items="${students}" var="studentItem">
    <tr>
        <td><c:out value="${studentItem.name}"></c:out></td>
        <td><c:out value="${studentItem.birthdate}"></c:out></td>
    </tr>
    </c:forEach>
</table>
</body>
</html>
