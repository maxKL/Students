package com.maximov.services;

import com.maximov.models.Student;
import com.maximov.repository.Repository;
import com.maximov.repository.StudentRepository;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by maxim_000 on 22.02.2017.
 */
public class StudentService {
    public List<Student> getList() throws SQLException {
        Repository rep = new StudentRepository();
        return rep.selectAll();
    }
}
